import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('olog_es')


def test_containers(host):
    with host.sudo():
        assert host.docker("olog-es").is_running
        assert host.docker("elasticsearch").is_running
        assert host.docker("mongodb").is_running
        assert host.docker("kafka").is_running
        assert host.docker("zookeeper").is_running


def test_api(host):
    cmd = host.run("curl --insecure --fail https://" + host.ansible.get_variables()['inventory_hostname'] + "/Olog/properties")
    assert cmd.rc == 0
    assert "\"state\":\"Active\"" in cmd.stdout
